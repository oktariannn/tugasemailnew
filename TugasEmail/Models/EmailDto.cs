﻿namespace TugasEmail.Models
{
    public class EmailDto
    {
        public string emailTo { get; set; } = string.Empty;
        public string Subject { get; set; } = string.Empty;
        public string notes { get; set; } = string.Empty;
    }
}
