﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace TugasEmail.Services.EmailService
{
    public interface IEmailService
    {
        void SendEmail(EmailDto request);
    }
}
